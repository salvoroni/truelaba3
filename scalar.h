#ifndef SCALAR_H
#define SCALAR_H
#include <stdint.h>
#include <stdlib.h>
int64_t scalar_prod(int a[], int b[], size_t size);
#endif
