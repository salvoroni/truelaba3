#include <stdint.h>
#include <stdio.h>
#include <inttypes.h>
#include "./prime.h"
#include "./scalar.h"

int first[] = {1,2,3,4,5};
int second[] = {6,7,8,9,10};

int main(int argc, char *argv[]){
	int64_t result = scalar_prod(first,second,sizeof(first));
	unsigned long prime;
	printf("result is %" PRId64 "\nnow input a unsigned long number -> ",result);
	scanf("%lu",&prime);
	if (is_prime(prime)){
		puts("the number is a prime");
	} else {
		puts("the number is not a prime");
	}
	return 0;
}

