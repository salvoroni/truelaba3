int is_prime(const unsigned long n){
	unsigned long counter;
	for (counter = 2; counter < n; counter++) {
		if ((n % counter) == 0) {
			return 0;
		}
	}
	return 1;
}
