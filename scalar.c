#include <stdint.h>
#include <stdlib.h>
int64_t scalar_prod(const int a[],const int b[], size_t size){
	int64_t sum = 0;
	for (unsigned i = 0; i < size; i++) {
		sum += a[i]*b[i];

	}
	return sum;
}
